import styled from 'styled-components';
import React from 'react';

const StyledButton = styled.button`
    color: red;
    font-size: 30px;
    font-weight: bold;
`;

const Button = ({className}) => {
    return (
        <StyledButton>Styled</StyledButton>
    )
}

export default Button
