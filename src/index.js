import React from 'react';
import ReactDOM from 'react-dom';

import Button from './button';

// main app
const App = () => {
    return (
        <div>
            <p>React</p>
            <Button />
        </div>
    );
};

export default App;

ReactDOM.render(<App />, document.getElementById('app'))
